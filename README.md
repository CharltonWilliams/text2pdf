# Text2PDF  

### **Text2PDF** is a Java application (JAR) that converts text input or text files to a PDFs. This will not work with Microsoft application(Word, Excel, etc).  

## How To Use  
### The Text2PDF can be ran through command-line on both Windows and Linux machines, therefore, _Java_, _C#_ and any other language will work. Location of the jar file does not matter, however, remember to include the directory and name of the jar in the command-line unless it is part of the environment. The Program takes in 3 Arguments. The arguments are:  

1. Body Type (T - Plain Text) (F - File)  
2. PDF Filename  
3. PDF Body (if Body is a file){Use a file name in args[2]}  

## Examples  
### Unix/Linux  
#### java -jar c:/JarLocation/Text2PDF.jar  "T" "/edi/work/TestText.pdf" "This is my Body"  
#### java -jar Text2PDF.jar "F" "/edi/work/TestFile.pdf" "/edi/scripts/archiveDelete.dat"  
### Windows  
#### Text2PDF.jar "T" "C:\Folder1\Folder2\Folder3\TestText.pdf" "This is my Body and It worked"  
#### Text2PDF.jar "F" "D:\Folder1\Folder2\TestFile.pdf" "C:\Folder1\Folder2\work\findFiles.pl"  

## Required Dependencies
* ### Java 8 or later is required

## Special Thanks
* [Sonoco EDI Team](https://www.sonoco.com)  
* [iText](https://itextpdf.com/en)  
    - For [Help](https://itextpdf.com/en/resources/api-documentation)  
    - For [AGPL Licence Compliance](https://itextpdf.com/en/blog/technical-notes/how-do-i-make-sure-my-software-complies-agpl-how-can-i-use-itext-free)  
   





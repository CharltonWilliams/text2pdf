import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Paragraph;
import java.io.*;

public class Text2PDF {
    public static void main(String[] args) throws FileNotFoundException {
        //args[0] - Body Type
        //args[1] - PDF Filename
        //args[2] - PDF Body (if Body is a file){Use a file name in args[2]}


        //Unix/Linux
        //"T" "/edi/work/TestText.pdf" "This is my Body and It worked"
        //"F" "/edi/work/TestFile.pdf" "/edi/scripts/archiveDelete.dat"

        //Windows
        //"T" "C:\Users\raven\Work\TestText.pdf" "This is my Body and It worked"
        //"F" "C:\Users\raven\Work\TestFile.pdf" "C:\Users\raven\Work\findFiles.pl"


        if(args.length<3){
            System.out.println("System Aborted due to Parameters");
            System.exit(0);
        }

        //Switch statment
        String bodyType = args[0];

        switch (bodyType){
            case "T": //T - Plain Text
                CreatePDF(args[1],args[2]);
                break;
            case "F": //F - File
                CreatePDF(args[1],File2Text(args[2]));
                break;
        }


    }

    public static String File2Text(String Filename){
        //Take file contents and accum into one String
        StringBuilder sb = new StringBuilder();

        try {
            BufferedReader br = new BufferedReader(new FileReader(Filename)); //read file

            String line;
            while((line = br.readLine())!=null){
                sb.append(line);//append file to a StringBuilder
                sb.append("\n");
            }
            br.close();
        }catch (IOException ex){
            ex.printStackTrace();
        }
        return sb.toString();
    }


    public static void CreatePDF(String filename,String text){

        String pdfDocument = filename;
//        if(new File(pdfDocument).exists()){
//            new File(pdfDocument).delete();
//        }
        try{
            PdfDocument pdf = new PdfDocument(new PdfWriter(pdfDocument));
            Document doc = new Document(pdf);
            doc.add(new Paragraph(text).setFontSize(12));//Add Contents to PDF doument
            doc.close();
        }catch (FileNotFoundException ex){
            ex.printStackTrace();
        }


        System.out.println("Complete");
    }
}
